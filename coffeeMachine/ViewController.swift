//
//  ViewController.swift
//  coffeeMachine
//
//  Created by Anatolii on 3/23/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var addBeansButton: UIButton!
    @IBOutlet weak var addWaterButton: UIButton!
    @IBOutlet weak var addMilkButton: UIButton!
    @IBOutlet weak var makeCapuccinoButton: UIButton!
    @IBOutlet weak var makeEspressoButton: UIButton!
    @IBOutlet weak var makeHotWaterButton: UIButton!
    @IBOutlet weak var outputTextFied: UITextView!
    
    let currCMachine = CoffeeMachine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeBorders()
        
    }
    
    func makeBorders() {
        
        addBeansButton.layer.masksToBounds = true
        addBeansButton.layer.borderWidth = 1
        addBeansButton.layer.borderColor = UIColor.blue.cgColor
        
        addWaterButton.layer.masksToBounds = true
        addWaterButton.layer.borderWidth = 1
        addWaterButton.layer.borderColor = UIColor.blue.cgColor
        
        addMilkButton.layer.masksToBounds = true
        addMilkButton.layer.borderWidth = 1
        addMilkButton.layer.borderColor = UIColor.blue.cgColor
        
        makeCapuccinoButton.layer.masksToBounds = true
        makeCapuccinoButton.layer.borderWidth = 1
        makeCapuccinoButton.layer.borderColor = UIColor.blue.cgColor
        
        makeEspressoButton.layer.masksToBounds = true
        makeEspressoButton.layer.borderWidth = 1
        makeEspressoButton.layer.borderColor = UIColor.blue.cgColor
        
        makeHotWaterButton.layer.masksToBounds = true
        makeHotWaterButton.layer.borderWidth = 1
        makeHotWaterButton.layer.borderColor = UIColor.blue.cgColor
        
        outputTextFied.layer.masksToBounds = true
        outputTextFied.layer.borderWidth = 1
        outputTextFied.layer.borderColor = UIColor.blue.cgColor
    }
    
    @IBAction func addBeansPressed(_ sender: Any) {
        showResult(currCMachine.addValue(CoffeeMachine.typeOfCMProperties.beans, 20))
    }
    
    @IBAction func addWaterPressed(_ sender: Any) {
        showResult(currCMachine.addValue(CoffeeMachine.typeOfCMProperties.water, 20))
    }
    
    @IBAction func addMilkPressed(_ sender: Any) {
        showResult(currCMachine.addValue(CoffeeMachine.typeOfCMProperties.milk, 20))
    }
    
    @IBAction func makeEspressoPressed(_ sender: Any) {
        showResult(currCMachine.makeDrink(CoffeeMachine.typeOfDrinks.espresso))
    }
    
    @IBAction func makeCapuccinoPressed(_ sender: Any) {
        showResult(currCMachine.makeDrink(CoffeeMachine.typeOfDrinks.capuccino))
    }
    
    @IBAction func makeHotWaterPressed(_ sender: Any) {
        showResult(currCMachine.makeDrink(CoffeeMachine.typeOfDrinks.hotWater))
    }
    
    func showResult(_ text: String) {
        outputTextFied.text = text
    }
    
}

