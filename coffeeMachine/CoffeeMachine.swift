//
//  CoffeeMachine.swift
//  coffeeMachine
//
//  Created by Anatolii on 3/23/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class CoffeeMachine: NSObject {
    
    private var ingridientsArray = ["beans": 0,
                                    "water": 0,
                                    "milk": 0]
    
    private let ingridientsMaxvalueArray = ["beansMaxValue": 100,
                                            "waterMaxValue": 200,
                                            "milkMaxValue": 150]
    
    private let ingridientsForDrinkArray = ["beansValueforDrink": 30,
                                            "waterValueforDrink": 40,
                                            "milkValueforDrink": 15]
    
    enum typeOfCMProperties: String {
        
        case beans = "beans"
        case water = "water"
        case milk = "milk"
    }
    
    enum typeOfDrinks: String {
        
        case espresso = "espresso"
        case capuccino = "capuccino"
        case hotWater = "hotWater"
    }
    
    enum answerCodes: Int {
        
        case valueAdded =  1
        case propertyIsFull = 2
        case notEnoughtProperty = 3
        case drinkIsDone = 4
    }
    
    func addValue(_ typeOfValue: typeOfCMProperties ,_ valueAmount: Int) -> String {
        
        let currValue = ingridientsArray[typeOfValue.rawValue] ?? 0
        let currValueMax = ingridientsMaxvalueArray[typeOfValue.rawValue + "MaxValue"] ?? 0
        if currValue + valueAmount > currValueMax {
            return returnAnswer(answerCodes.propertyIsFull, typeOfValue.rawValue)
        }
        else {
            changeAmount(typeOfValue.rawValue, valueAmount, true)
            return returnAnswer(answerCodes.valueAdded, typeOfValue.rawValue)
        }
    }
    
    func makeDrink(_ drink: typeOfDrinks) -> String {
        
        var arrayOfPropertiesInUse = [String]()
        
        if drink == typeOfDrinks.capuccino {
            arrayOfPropertiesInUse.append(typeOfCMProperties.beans.rawValue)
            arrayOfPropertiesInUse.append(typeOfCMProperties.water.rawValue)
            arrayOfPropertiesInUse.append(typeOfCMProperties.milk.rawValue)
        }
        else if drink == typeOfDrinks.espresso {
            arrayOfPropertiesInUse.append(typeOfCMProperties.beans.rawValue)
            arrayOfPropertiesInUse.append(typeOfCMProperties.water.rawValue)
        }
        else if drink == typeOfDrinks.hotWater {
            arrayOfPropertiesInUse.append(typeOfCMProperties.water.rawValue)
        }
        var answer = chekIfPropertiesEnough(arrayOfPropertiesInUse)
        
        if answer == "" {
            for prop in arrayOfPropertiesInUse {
                let amountForDrink = ingridientsForDrinkArray[prop + "ValueforDrink"] ?? 0
                changeAmount(prop, amountForDrink, false)
            }
            answer = returnAnswer(answerCodes.drinkIsDone, drink.rawValue)
        }
        return answer
    }
    
    private func returnAnswer(_ caseValue: answerCodes, _ prop: String) -> String {
        
        let currValue = caseValue.rawValue
        var answer = ""
        
        switch currValue {
        case 1:
            let currValueAmount = ingridientsArray[prop] ?? 0
            let currValueMaxAmount = ingridientsMaxvalueArray[prop + "MaxValue"] ?? 0
            answer = """
            \(prop) are added.
            number of \(prop): \(currValueAmount) \\ \(currValueMaxAmount))
            """
        case 2:
            answer = "\(prop) is already full!"
        case 3:
            answer = "It is not enought of \(prop) for your drink. Please add \(prop)"
        case 4:
            answer = "Your drink is done. Enjoy your \(prop)"
        default:
            answer = "error has occured, please check your command type!"
        }
        return answer
    }
    
    private func chekIfPropertiesEnough(_ arrayOfPropertiesInUse: [String]) -> String {
        var answer = ""
        for prop in arrayOfPropertiesInUse {
            let currIngr = ingridientsArray[prop] ?? 0
            let currDrinkIngr = ingridientsForDrinkArray[prop+"ValueforDrink"] ?? 0
            if currIngr <  currDrinkIngr {
                answer += returnAnswer(answerCodes.notEnoughtProperty, prop)
            }
        }
        return answer
    }
    
    private func changeAmount(_ typeOfValue: String, _ valueAmount: Int, _ increasing: Bool) {
        
        var currIngridient = self.ingridientsArray[typeOfValue] ?? 0
        currIngridient = increasing ? currIngridient + valueAmount : currIngridient - valueAmount
        self.ingridientsArray[typeOfValue] = currIngridient
    }
}
